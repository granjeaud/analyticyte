
# analyticyte

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.HTML#experimental)
<!-- badges: end -->

## Installation

You can install the development version of analyticyte with:

``` r
remotes::install_git("https://gitcrcm.marseille.inserm.fr/lohmann/analyticyte)
```

## To do

- [ ] Evaluation server shiny

- [x] Importer le concat fcs

- [x] Extraire les features du concat

- [ ] Univarié apparié

- [ ] Sauvegarde auto du rds

- [ ] Modification des couleurs parametres patient

- [x] Rédaction du template DS

- [x] Création d’une Heatmap Abondance/MFI en QC

- [x] Correction du format interne des fonction tbl_mfi (scaled_mfi)

- [ ] Export des formats Phantasus +/- annotés

- [ ] Page d’inspection features

- [x] Annotation des cluster sur le HTML DA

- [ ] Verification des transformations format fichier long, colonne fcs
  deja referencé dans shared_col mais pas la colonne de valeurs

- [ ] Permettre la modification des metadata dans l’application

- [ ] Permettre le subsample des données dans l’application
  (concentration sur des markers d’intérêt, sur des clusters d’intérêt)

- [x] Export des figures en SVG sur les HTML

- [ ] retrouver le chemin et le nom du fichier si on load RDS dans
  l’application

- [ ] Prendre en compte des comparaisons facteurs \> 2 parametres

- [x] ! gérer les rapports sans résultats significatifs

- [ ] Supperposition des heatmaps de MFIs selon les conditions

- [ ] Graphique de points (q Amira formation)

- [ ] Gerer l’ensembles des packages nécessaires à l’installation de
  l’application + rapports

- [x] écriture des rapports sous windows

- [x] suivi de <https://github.com/quarto-dev/quarto-cli/issues/5702>
  Error in add_HTML_caption(): ! unused argument
  (xfun::grep_sub(“^\[^\<\]*\<\[^\>\]+aria-labelledby\[ \]*=\[
  \]*"(\[^\\\]+)".*\$”, “\1”, x))
