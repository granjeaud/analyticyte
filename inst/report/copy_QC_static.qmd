---
engine: knitr
---

# Number of cells Barplot

::: column-margin

- Check the number of cells in each fcs.

- Controls the possible association with a batch effect.

Bars are sorted by conditions. Annotations columns are added on the barplot's top to control possible batch effects.

::: 

```{r, class.chunk="bars-all", fig.width = 20,  fig.height = desired_height*.75}
#| label: fig-barplot-QC
#| fig-cap: Barplot number of cells per fcs


all_n_cells <- getFeature(se, c("assays", "counts")) |>
  colSums() |>
  sort()
outliers <- all_n_cells[all_n_cells != max(all_n_cells)] |> head()

names_outliers <- outliers |> names()


AnalytiCFX::barplot_n_cells(
  se = se,
  t_metadata = t_metadata,
  counts = size_,
  Conditions = Conditions,
  id = id,
  sorted = sorted,
  col_interest = col_interest,
  col_cell = col_cell,
  batch = batch
)
```



Less cells fcs : `r paste(names_outliers, ' (',outliers,' cells)', sep="")`


```{r, class.chunk="bars-all", fig.width=10, fig.height = desired_height*.5}
#| label: fig-barplot-QC-cluster
#| fig-cap: Barplot number of cells per clusters

ggplot(as.data.frame(info_row), aes(x = !!sym(c_id), y = percent.n_cells)) +
  theme_minimal() +
  geom_bar(stat = "identity", col = "steelblue", fill = "steelblue") +
  theme(
    axis.text.x = element_text(angle = 45, hjust = 1),
    panel.grid.major.x = element_blank()
  ) +
  scale_x_discrete(limits = as.data.frame(info_row) |> arrange(desc(percent.n_cells)) |> pull(!!sym(c_id))) +
  labs(y = "Percentage of total cells", x = "Cluster ID") +
  theme_classic()


total_c <- sum(info_row$n_cells)
rep_homo <- total_c / nrow(info_row)
info_row |>
  subset(n_cells < rep_homo) |>
  as.data.frame() |>
  arrange(n_cells) |>
  head() -> sub_info_row
```

Less cells clusters : `r paste(sub_info_row$cluster_id, ' (',sub_info_row$percent.n_cells,'%)', sep="")`


# Density Heatmap

A special distance method, denoted as 'ks,' measures the similarity between distributions by computing the Kolmogorov-Smirnov statistic between two distributions.

```{r, fig.width=20}
#| label: fig-density-plot
#| fig-cap: "Density heatmap of perCellCountsNorm clustered by ks"


percellcount <- getFeature(se, c("assays", "perCellCountsNorm"))

ComplexHeatmap::densityHeatmap(percellcount,
  cluster_columns = TRUE, clustering_distance_columns = "ks", top_annotation = top, heatmap_legend_param = list(
    legend_direction = "horizontal"
  ), use_raster = TRUE,
  raster_by_magick = TRUE
  # raster_quality = 5
) -> d_heat

draw(d_heat, heatmap_legend_side = "bottom")
```



# MFI's Heatmap

::: column-margin

-  Check or help annotate clusters.

The input is the MFI cluster x marker array. The MFI is already transformed (typically using asinh(intensity/cofactor)).

Heatmap of the MFI with clusters in rows and markers in columns.

A barplot shows the number of cells in each metacluster. 

The first heatmap covers all fcs.

::: 

```{r}
AnalytiCFX::heatmap_median_mfi_like_catalyst(
  # ajouter la possibilité de trier les colonnes, rows
  se = se,
  id = id,
  exprs = "counts",
  se_mfi = se_mfi,
  subset_marker = NULL, # reduction of marker of interest, order of the heatmap if clustr F
  subset_cluster = NULL, # reduction of clusters of interest, order of the heatmap if clustc F
  clustr = T,
  clustc = T,
  margin_ = 2,
  t_metadata = t_metadata,
  split_heat = NULL,
  q_ = 0.1,
  fontsize_ = 8,
  round_ = 8,
  applied_fct = "mean",
  log_bar = 0,
  title = "All Samples"
) -> all_plot_mfi_heatmap

order_c <- ComplexHeatmap::column_order(all_plot_mfi_heatmap)
order_r <- ComplexHeatmap::row_order(all_plot_mfi_heatmap)
```



```{r}
#| layout-ncol: 2
#
#
#
#
# Let's say our group comparison is based on the metadata column `r col_interest`, the different groups are : `r unique(metadata(se)[[t_metadata]][[col_interest]])`

#
subgroup <- unique(metadata(se)[[t_metadata]][[col_interest]])
# second_plot <- FALSE
markers_list <- names(assays(se_mfi))
cluster_list <- names(se_mfi)
for (sub_el in subgroup) {
  AnalytiCFX::heatmap_median_mfi_like_catalyst(
    # ajouter la possibilité de trier les colonnes, rows
    se = se,
    id = id,
    exprs = "counts", # perCellCounts
    se_mfi = se_mfi,
    subset_marker =
    # { if (second_plot)
      markers_list[order_c]
    # else markers_list}
    # reduction of marker of interest, order of the heatmap if clustC F
    , subset_cluster =
    # {
    # if (second_plot)
      cluster_list[order_r]
    # order_r
    # else cluster_list }
    # reduction of clusters of interest, order of the heatmap if clustR
    , clustr =
    # {  if (second_plot)
      F
    # else T}
    , clustc =
    # {  if (second_plot)
      F
    # else T },
    , margin_ = 2,
    t_metadata = t_metadata,
    split_heat = c(separator_, sub_el),
    q_ = 0.1,
    fontsize_ = 8,
    round_ = 8,
    applied_fct = "mean",
    log_bar = 0,
    title = sub_el
  ) -> plot
  #   |> ComplexHeatmap::draw(
  #   column_title = sub_el,
  #   column_title_gp = grid::gpar(fontsize = 16)
  # )


  # second_plot <- TRUE
  # new_order_c <- ComplexHeatmap::column_order(plot)
  # new_order_r <- ComplexHeatmap::row_order(plot)
}
```




# Mfi x Abundance Heatmap


::: column-margin

- Heatmap grouping the MFI of each marker per cluster and cluster abundance per fcs.

- MFIs are normalized per line between 0 and 1 to give a comparable distribution of marker expression per cluster.

- Markers are grouped hierarchically to facilitate cluster reading/interpretation.

Relative abundances shown on the second heat map are transformed abundances relative to the cluster mean. The green-purple color scale applies to the entire matrix. 

::: 

```{r , fig.width = desired_width*1.5,  fig.height = desired_height*3}
#| label: fig-heat-cytofast-plot
#| fig-cap: "Abundance heatmap with clusters associated with mfi"


AnalytiCFX::heatmap_like_cytofast(
  se = se, # se=dataSE$SE_abundance
  exprs = "counts", #  assays name reference in se -> counts
  id = id, # sample id accross metadata "shared_col"
  se_mfi = se_mfi, # a SummarizedExperiment (se) object with mfi info   se_mfi=dataSE$SE_mfi
  scaled_MFI = scaled_MFI, # Name of metadata, if the SE scaled_MFI data
  marker_id = marker_column_id, # name of the cluster column name
  # metadata_sub=c("acqdate","Gender","patient_id"),
  metadata_sub = to_publish,
  id_mfi = "shared_col",
  t_metadata = t_metadata,
  metadata_sort = NULL,
  clustr = F,
  clustc = F,
  clustr_abu = T,
  fontsize_ = 8,
  subset_patient = NULL, # reduction of patients of interest, order of the heatmap if clustr F
  subset_marker = markers_list[order_c], #  order_r, # reduction of marker of interest, order of the heatmap if clustr F
  subset_cluster = cluster_list[order_r], # reduction of clusters of interest, order of the heatmap if clustc F
  round_ = 8,
  applied_fct = "mean" # applied_fct_plot
)
```


# Abundance Heatmap

::: column-margin

-   Visualize samples organized according to hierarchical grouping or by condition.

-   Check that sample states are homogeneous; identify outliers or gender effects.

-   Check whether conditions have already been separated.

Percentage heatmap with samples in columns and clusters in rows.

By default, clusters and samples are grouped using hierarchical clustering. It is also possible to classify samples by condition, gender or patient, and to give clusters as an ordered vector.

Group abundance is shown in the right-hand barplot with log values.

::: 


```{r, fig.width = desired_width*3,  fig.height = desired_height*1.2}
#| label: fig-heat-abundance-plot-unsupervised
#| fig-cap: "Abundance Heatmap"

AnalytiCFX::heatmap_abundance_like_catalyst(
  # ajouter la possibilité de trier les colonnes, rows
  se = se,
  exprs = "perCellCountsNorm",
  t_metadata = t_metadata,
  separator_ = separator_,
  # metadata_sub=c("acqdate","patient_id","cond"),
  metadata_sub = to_publish,
  patient = patient,
  id = id,
  # metadata_sort = "cond",
  clustr = T,
  clustc = T,
  margin_ = 1,
  q_ = 0.05,
  round_ = 8,
  fontsize_ = 8,
  subset_patient = NULL, # reduction of patients of interest, order of the heatmap if clustr F
  subset_cluster = NULL, # reduction of clusters of interest, order of the heatmap if clustc F
  log_bar = 0
)
```




# PCA of samples using percentages per cluster

::: column-margin

-   Visualize samples in a reduced space.

-   Check that sample states are homogeneous; identify outliers or gender effects.

-   Check whether conditions can already be separated.

-   Based on the `ade4`[@ArtADE42004] dudi.pca function.


:::


```{r}
PCA_SE <- AnalytiCFX::patient_to_pca(
  pca = T,
  need_centering = TRUE,
  need_scale = FALSE,
  # nf_ = 3,
  nf_ = 5,
  scaled_MFI = NULL,
  se = se,
  exprs = "perCellCountsNorm",
  t_metadata = t_metadata,
  id = "shared_col",
  round_ = 8,
  NA_to_0 = TRUE
)
```

::: column-margin

Input is the percentages table sample x cluster. Percentages are previously transformed with asinh(%+cte) with cte = 0.03 and centered by cluster.


-   check that the first 2 or 3 components retains the main information

-   Graphical figures generated with `factoextra`[@ManuFactoextra] functions.


::: 



## Barplot of variance of each PC

```{r , fig.width=10,fig.height=3}
factoextra::fviz_eig(PCA_SE$dpca, addlabels = TRUE) +
  theme_classic()
```

### Axe 1 vs Axe 2

```{r}
#| layout-ncol: 2

factoextra::fviz_pca_ind(PCA_SE$dpca,
  axes = c(1, 2),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = {
    if (!is.null(batch)) PCA_SE$metadata[[batch]] else PCA_SE$metadata[[Conditions]]
  },
  palette = {
    if (!is.null(batch)) colors_to_use[[batch]] else colors_to_use[[Conditions]]
  },
  addEllipses = TRUE,
  legend.title = {
    if (!is.null(batch)) batch else Conditions
  }
) + theme(legend.position = "bottom") |> suppressWarnings()


factoextra::fviz_pca_ind(PCA_SE$dpca,
  axes = c(1, 2),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = PCA_SE$metadata[["expdate"]],
  palette = colors_to_use[["expdate"]],
  ellipse.level = 0.75,
  addEllipses = TRUE,
  legend.title = "expdate"
) + theme(legend.position = "bottom") |> suppressWarnings()
```

```{r, fig.width=10}
factoextra::fviz_pca_biplot(PCA_SE$dpca,
  repel = FALSE,
  axes = c(1, 2),
  # col.ind = "dimgray",
  col.ind = "dimgray",
  col.var = "red"
)
```



### Axe 1 vs Axe 3


```{r}
#| layout-ncol: 2

factoextra::fviz_pca_ind(PCA_SE$dpca,
  axes = c(1, 3),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = {
    if (!is.null(batch)) PCA_SE$metadata[[batch]] else PCA_SE$metadata[[Conditions]]
  },
  palette = {
    if (!is.null(batch)) colors_to_use[[batch]] else colors_to_use[[Conditions]]
  },
  addEllipses = TRUE,
  legend.title = {
    if (!is.null(batch)) batch else Conditions
  }
) + theme(legend.position = "bottom") |> suppressWarnings()



factoextra::fviz_pca_ind(PCA_SE$dpca,
  axes = c(1, 3),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = PCA_SE$metadata[["expdate"]],
  palette = colors_to_use[["expdate"]],
  ellipse.level = 0.75,
  addEllipses = TRUE,
  legend.title = "expdate"
) + theme(legend.position = "bottom") |> suppressWarnings()
```



```{r, fig.width=10}
factoextra::fviz_pca_biplot(PCA_SE$dpca,
  repel = FALSE,
  axes = c(1, 3),
  col.ind = "dimgray",
  col.var = "red"
)
```

### Axe 2 vs Axe 3



```{r}
#| layout-ncol: 2

factoextra::fviz_pca_ind(PCA_SE$dpca,
  axes = c(2, 3),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = {
    if (!is.null(batch)) PCA_SE$metadata[[batch]] else PCA_SE$metadata[[Conditions]]
  },
  palette = {
    if (!is.null(batch)) colors_to_use[[batch]] else colors_to_use[[Conditions]]
  },
  addEllipses = TRUE,
  legend.title = {
    if (!is.null(batch)) batch else Conditions
  }
) + theme(legend.position = "bottom") |> suppressWarnings()



factoextra::fviz_pca_ind(PCA_SE$dpca,
  axes = c(2, 3),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = PCA_SE$metadata[["expdate"]],
  palette = colors_to_use[["expdate"]],
  ellipse.level = 0.75,
  addEllipses = TRUE,
  legend.title = "expdate"
) + theme(legend.position = "bottom") |> suppressWarnings()
```

```{r, fig.width=10}
factoextra::fviz_pca_biplot(PCA_SE$dpca,
  repel = FALSE,
  axes = c(2, 3),
  col.ind = "dimgray",
  col.var = "red"
)
```

## Contribution of variables to each axis

```{r}
AnalytiCFX::create_dt(round(factoextra::get_pca_var(PCA_SE$dpca)$contrib, 4), length = 5, rownames_ = T, format = out_format)
```









# PCA of samples using MFI

Plot variables in order to identify their contribution to each PC

::: column-margin

PCA of samples using MFI per cluster Similar to Nowicka et al., but at the clustering level (not the cell level).

-   View the samples in a reduced space.

-   Verify that States of samples are homogeneous; identify outliers or Gender effects.

-   Check if the conditions could already be separated.

Input is the MFI table sample x cluster. MFI are previously transformed.

-   Based on the `ade4`[@ArtADE42004] dudi.pca function.

::: 

```{r, fig.width=2,fig.height=3}
PCA_MFI <- AnalytiCFX::patient_to_pca(
  pca = T,
  need_centering = TRUE,
  need_scale = FALSE,
  # nf_ = 3,
  nf_ = 5,
  scaled_MFI = scaled_MFI,
  se = se_mfi,
  exprs = "counts",
  t_metadata = t_metadata,
  id = "shared_col",
  round_ = 8,
  NA_to_0 = TRUE
)
```




```{r, fig.width=10,fig.height=3}
factoextra::fviz_eig(PCA_MFI$dpca, addlabels = TRUE) +
  theme_classic()
```

### Axe 1 vs Axe 2


```{r}
#| layout-ncol: 2

factoextra::fviz_pca_ind(PCA_MFI$dpca,
  axes = c(1, 2),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = {
    if (!is.null(batch)) PCA_MFI$metadata[[batch]] else PCA_MFI$metadata[[Conditions]]
  },
  palette = {
    if (!is.null(batch)) colors_to_use[[batch]] else colors_to_use[[Conditions]]
  },
  ellipse.level = 0.75,
  addEllipses = TRUE,
  legend.title = {
    if (!is.null(batch)) batch else Conditions
  }
) + theme(legend.position = "bottom")



factoextra::fviz_pca_ind(PCA_MFI$dpca,
  axes = c(1, 2),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = PCA_MFI$metadata[["expdate"]],
  palette = colors_to_use[["expdate"]],
  ellipse.level = 0.75,
  addEllipses = TRUE,
  legend.title = "expdate"
) + theme(legend.position = "bottom")
```


```{r, fig.width=10}
factoextra::fviz_pca_biplot(PCA_MFI$dpca,
  repel = FALSE,
  axes = c(1, 2),
  col.ind = "dimgray",
  col.var = "red"
)
```


### Axe 1 vs Axe 3


```{r}
#| layout-ncol: 2

factoextra::fviz_pca_ind(PCA_MFI$dpca,
  axes = c(1, 3),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = {
    if (!is.null(batch)) PCA_MFI$metadata[[batch]] else PCA_MFI$metadata[[Conditions]]
  },
  palette = {
    if (!is.null(batch)) colors_to_use[[batch]] else colors_to_use[[Conditions]]
  },
  ellipse.level = 0.75,
  addEllipses = TRUE,
  legend.title = {
    if (!is.null(batch)) batch else Conditions
  }
) + theme(legend.position = "bottom")


factoextra::fviz_pca_ind(PCA_MFI$dpca,
  axes = c(1, 3),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = PCA_MFI$metadata[["expdate"]],
  palette = colors_to_use[["expdate"]],
  ellipse.level = 0.75,
  addEllipses = TRUE,
  legend.title = "expdate"
) + theme(legend.position = "bottom")
```

```{r, fig.width=10}
factoextra::fviz_pca_biplot(PCA_MFI$dpca,
  repel = FALSE,
  axes = c(1, 3),
  col.ind = "dimgray",
  col.var = "red"
)
```


### Axe 2 vs Axe 3


```{r}
#| layout-ncol: 2
factoextra::fviz_pca_ind(PCA_MFI$dpca,
  axes = c(2, 3),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = {
    if (!is.null(batch)) PCA_MFI$metadata[[batch]] else PCA_MFI$metadata[[Conditions]]
  },
  palette = {
    if (!is.null(batch)) colors_to_use[[batch]] else colors_to_use[[Conditions]]
  },
  ellipse.level = 0.75,
  addEllipses = TRUE,
  legend.title = {
    if (!is.null(batch)) batch else Conditions
  }
) + theme(legend.position = "bottom")


factoextra::fviz_pca_ind(PCA_MFI$dpca,
  axes = c(2, 3),
  geom = "text",
  # col.ind = "cos2", # Color by the quality of representation
  repel = FALSE, # Avoid text overlapping
  col.ind = PCA_MFI$metadata[["expdate"]],
  palette = colors_to_use[["expdate"]],
  ellipse.level = 0.75,
  addEllipses = TRUE,
  legend.title = "expdate"
) + theme(legend.position = "bottom")
```

```{r, fig.width=10}
factoextra::fviz_pca_biplot(PCA_MFI$dpca,
  repel = FALSE,
  axes = c(2, 3),
  col.ind = "dimgray",
  col.var = "red"
)
```


## Contribution of variables to each axis

```{r}
AnalytiCFX::create_dt(
  x = round(factoextra::get_pca_var(PCA_MFI$dpca)$contrib, 4),
  length = 5,
  rownames_ = T,
  format = out_format,
  cut = 10
)
```
