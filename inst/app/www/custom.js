$(document).ready(function() {
    var isSidebarOpen = true;

    $('#historyToggle').on('click', function() {
        // Toggle the sidebar state
        if (isSidebarOpen) {
            $('#rightSidebar').removeClass('active').addClass('inactive');
            $('#main-content').css('margin-right', '0');
            $("#historyArrow").text(String.fromCharCode(0xf105));  // Replaces ">"
        } else {
            $('#rightSidebar').removeClass('inactive').addClass('active');
            $('#main-content').css('margin-right', '250px');
            $("#historyArrow").text(String.fromCharCode(0xf107));  // Replaces "v"
        }
        
        isSidebarOpen = !isSidebarOpen;
        Shiny.setInputValue('isSidebarOpen', isSidebarOpen);
    });
});

  Shiny.addCustomMessageHandler('closeTab', function(message) {
    window.close();
  });
  
  $(document).on('shiny:connected', function(event) {
  Shiny.addCustomMessageHandler("toggleInTablesMenu", function(message) {
    if (message === true) {
      $(".element_hee").show();
    } else {
      $(".element_hee").hide();
    }
  });
});
